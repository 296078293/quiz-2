import $ from 'jquery';

export function renderAboutMe(person) {
  $('#description').html(person.description);
}
