export function fetchData(url) {
  return fetch(url).then(x => x.json());
}
