export default class Education {
  constructor(year, title, description) {
    this._year = year;
    this._title = title;
    this._description = description;
  }
  get year() {
    return this._year;
  }

  get title() {
    return this._title;
  }

  get description() {
    return this._description;
  }

  set year(value) {
    this._year = value;
  }

  set title(value) {
    this._title = value;
  }

  set description(value) {
    this._description = value;
  }
}
