import $ from 'jquery';

export function renderEducations(person) {
  person.educations.map(education => {
    $('#educations').append(
      `<li>
<h3 class="year">${education.year}</h3>
<div class="detail"><h3 class="title">${education.title}</h3>
<p>${education.description}</p>
</div>
</li>`
    );
  });
}
