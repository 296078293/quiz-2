import Education from './Education';

export default class Person {
  constructor(name, age, description, educations) {
    this._name = name;
    this._age = age;
    this._description = description;
    this._educations = educations.map(education => {
      const { year, title, description } = education;
      return new Education(year, title, description);
    });
    this._educations = educations;
  }
  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get description() {
    return this._description;
  }

  get educations() {
    return this._educations;
  }
}
