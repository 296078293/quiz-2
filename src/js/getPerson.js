import Person from './Person';

export function createPerson(result) {
  const person = new Person(
    result.name,
    result.age,
    result.description,
    result.educations
  );
  return person;
}
