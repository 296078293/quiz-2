import { createPerson } from './getPerson';
import { renderEducations } from './render/renderEducations';
import { renderHeader } from './render/renderHeader';
import { renderAboutMe } from './render/renderAboutMe';
import { fetchData } from './fetchData';

const URL = 'http://localhost:3000/person';

fetchData(URL).then(result => {
  const person = createPerson(result);
  renderHeader(person);
  renderAboutMe(person);
  renderEducations(person);
});
