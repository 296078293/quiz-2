import $ from 'jquery';

export function renderHeader(person) {
  $('#name').html(person.name);
  $('#age').html(person.age);
}
